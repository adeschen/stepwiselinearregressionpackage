# **StepwiseLinearReg** #

R package implementing an automatic forward stepwise linear regression using either the adjusted R-squared (also known as the adjusted coefficient of determination) or the Akaike information criterion (AIC).

Un package R implémentant une procédure automatisée de sélection de variables à l'aide d'une régression linéaire multiple qui utilise soit le coefficient de détermination ajusté, soit le critère d’information d'Akaike (AIC).

## ** Author** ##

[Astrid Deschenes](http://ca.linkedin.com/in/astriddeschenes "Astrid Louise Deschenes")

## **License** ##

This package and the underlying StepwiseLinearReg code are distributed under the GNU General Public License version 3. You are free to use and redistribute this software under the same license terms.

For more information on  GNU General Public License see https://www.gnu.org/copyleft/gpl.html