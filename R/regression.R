#' @title Regression Automatic Procedure
#' 
#' @description Stepwise linear regression automatic procedure using either
#'      the adjusted coefficient of determination (also known as the adjusted 
#'      R-squared) or the Akaike information criterion (AIC) to select the best
#'      model. The forward stepwise procedure 
#'      starts with a linear regression without variable and 
#'      tests the addition of each available variable separatly. The variable
#'      giving the best result is added to the linear model and the procedure 
#'      is done again up to the moment when the addition of an available  
#'      variable doesn't give better result or that all availables variables 
#'      have been added. A new variable is added only if the improvement of the
#'      model is superior to 1e-07.
#' 
#' @param y a \code{vector} containing the outcome values
#' @param explanatoryVars an \code{array} or a \code{data.frame} where each
#'          column represent a potential explanatory variable that could be
#'          add to the linear regression. The number of rows must correspond
#'          to the length of the \code{y} parameter.
#' @param criterion a \code{character} indicating which parameter should be
#'          used to select the best model. The choices are "AIC" 
#'          (Akaike information criterion) and "R2ajuste" (adjusted R-squared).
#'          Default: "AIC".
#' @param verbose a \code{logical} value indicating if trace should be printed 
#'          during the procedure. Default: \code{TRUE}.
#' 
#' @return a list is returned for use by \code{print.regression}. This list has
#'            the components: 
#'            \itemize{
#'                \item{\code{iterations}}{ a \code{data.frame} containing 
#'                        important information for each step of the procedure.
#'                        The \code{data.frame} has tree columns :
#'                          \itemize{
#'                            \item{\code{Iteration}}{ a \code{integer} 
#'                                identifying the iteration }
#'                            \item{\code{Step}}{ a \code{character} 
#'                                identifying the name of the added variable}
#'                            \item{\code{AIC} or \code{R2ajuste}}{ a 
#'                                \code{numeric} giving the value of the 
#'                                criterion}
#'                          }
#'                     }
#'                \item{\code{lm}}{ an object of \code{class} "lm" which is a  
#'                        list containing elements related to the regression 
#'                        model. The function \code{summary} is used to obtain  
#'                        and print a summary of the results. }
#'            }
#' 
#' @author Astrid Louise Deschenes
#' @importFrom stats lm update.formula as.formula extractAIC
#' @importFrom utils flush.console
#' @keywords regression
#' @seealso \code{\link{EnergyEfficiency}}
#' @export
#' @examples
#' data(EnergyEfficiency)
#' reg<-regression(EnergyEfficiency$HeatingLoad, EnergyEfficiency[,1:7], criterion="R2ajuste")
#' reg
#' summary(reg$lm)
regression <- function(y, explanatoryVars, criterion = c("AIC", "R2ajuste"), 
                       verbose = TRUE) {
  
  ####################################################
  # Parameters validation
  ####################################################
  # y has to be a numerical vector
  if(!is.vector(y)) {
    stop("'y' must be a numerical vector")
  } 
  
  # Ensure that the vector is unitary and numerical
  y <- unlist(y)
  if(!is.numeric(y)) {
    stop("'y' must be a numerical vector")
  }
  
  # explanatoryVars has to be either a array of 2 dimensions or a data.frame
  if(!(is.array(explanatoryVars) && length(dim(explanatoryVars)) == 2 && 
          is.numeric(explanatoryVars)) && !is.data.frame(explanatoryVars)) {
    stop("'explanatoryVars' must be a numerical array of two dimensions or a data frame")
  }
  
  # when explanatoryVars is a data.frame, all its columns have to be numerical
  if (is.data.frame(explanatoryVars) && 
          !all(sapply(explanatoryVars, is.numeric))) {
    stop("'explanatoryVars' must only contain numerical data")
  }
  
  # verbose has to be a logical value
  if (!is.logical(verbose)) {
    stop("'verbose' must be a logical value")
  }
  
  # The number of rows of explanatoryVars has to be equal to the lenght of y
  if (length(y) != nrow(explanatoryVars)) {
    stop("'explanatoryVars' must have a number of rows corresponding to the length of 'y'")
  }
  
  # Validate criterion value
  criterion <- match.arg(criterion)
  
  ####################################################
  # Variables preparation
  ####################################################
  
  # Give names to column when not name assign
  if (is.null(colnames(explanatoryVars))) {
    colnames(explanatoryVars)<-paste0("V", 1:ncol(explanatoryVars))
  }
  
  # Transform array to data frame
  if (is.array(explanatoryVars)) {
    explanatoryVars <- as.data.frame(explanatoryVars)
  }
  
  # Create data containing both response and terms values
  data <- cbind(y = y, explanatoryVars)
  
  ####################################################
  # Main core
  ####################################################

  # Current regression formula
  # The first regression only use an y-intercept
  formula <- as.formula("y ~ 1")
  
  # First linear regression using only an y-intercept 
  regression <- getRegression(formula, data)
  
  # Get the criterion value for the initial regression
  criterionValue <- getRegressionCriterionValue(regression, criterion)
 
  # List which will contain all criterion values at each step of the stepwise
  # regression procedure
  values <- criterionValue
  names(values) <- "(Intercept)"
  
  # Terms that can be used in the linear regression
  unusedTerms <- names(explanatoryVars)
 
  # Logical variable used to determine if regression must continue
  # another step forward
  isBetterFit <- TRUE
  
  # Logical variable used to dtermine if it is the first loop or not
  isFirst <- TRUE
  
  while (isBetterFit && length(unusedTerms) != 0) {
    # Get all combinaisons of regressions using the current formula added
    # with one left term
    regResults <- sapply(unusedTerms, function(term) getRegression(data = data, 
                          formula = formula, newTerm = term), simplify = FALSE)
    # Get the criterion values for all regressions
    regValues <- lapply(regResults, getRegressionCriterionValue, 
                          criterion = criterion)
    
    # Print trace to console
    if (verbose) {
      printTrace(formula, criterionValue, regValues, criterion, isFirst)
      isFirst <- FALSE
    }
    
    # Get the position of the regression which gave the best result for 
    # the selected criterion
    # When more than one regression has the best result, the first one in
    # the list is selected
    position <- ifelse(criterion == "AIC", which.min(regValues), 
                        which.max(regValues)) 
    # Get the criterion value of the selected regression
    criterionValue <- unlist(regValues[position])

    # When criterion value is -Inf, the procedure must be stopped
    if (criterion == "AIC" && is.infinite(criterionValue)) {
      stop(paste0("AIC is -Infinity for this model, the forward stepwise ", 
              "linear regression automatic procedure cannot proceed"))
    }
    # When criterion value is NaN, the procedure must be stopped
    if (criterion == "R2ajuste" && all(is.nan(unlist(regValues)))) {
      stop(paste0("Adjusted R-squared is NaN for this model, the forward ", 
              "stepwise linear regression automatic procedure cannot proceed"))
    }
    
    # Check if the selected regression has a better fit than the initial
    # formula without the added term
    isBetterFit <- ifelse(criterion == "AIC", 
                          values[length(values)] - criterionValue  >  1e-07, 
                          criterionValue - values[length(values)]  >  1e-07)
    
    # When the selected regression is better, the variables used in the
    # loop need to be update
    # When the selected regression is not better, the loop will end
    if (isBetterFit) {
      # The term added to the formula must be deleted of the current list of
      # unselected terms
      unusedTerms <- subset(unusedTerms, unusedTerms != names(criterionValue))
      # Add the new criterion value to the list contain criterion values at 
      # each step of the stepwise regression procedure
      values <- c(values, criterionValue)
      # Update regression with the selected one
      regression <- regResults[[position]]
      # Add the new term to the formula
      formula <- update.formula(formula, paste("~ . + ", names(criterionValue)))
    }
  }
  
  # Prepare data frame which contains a summary of each step
  final <- data.frame(Iteration=1:length(values), Step=names(values), 
                        row.names = NULL)
  final[criterion]<-values
  
  # Return a list marked as an regression class containing :
  # 1- a list containing the summary data frame 
  # 2- the final regression 
  out <- list(iterations=final, lm=regression)
  class(out) <- "regression"
  return(out)
}


